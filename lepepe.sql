use lepepe;
create table if not exists cities (
	title varchar(200) not null,
    woeid varchar(10) not null,
    latt_long varchar(200) not null,
    timezone varchar(200) not null,
    sun_rise varchar(200) not null,
    sun_set varchar(200) not null,
    unique key idid (woeid)
);
create table if not exists citydata (
	id varchar(200) not null,
    woeid varchar(200) not null,
	weather_state_name varchar(200) not null,
	weather_state_abbr varchar(200) not null,
	wind_direction_compass varchar(200) not null,
	created varchar(200) not null,
	applicable_date varchar(200) not null,
	min_temp varchar(200) not null,
	max_temp varchar(200) not null, 
	the_temp varchar(200) not null,
	wind_speed varchar(200) not null,
	wind_direction varchar(200) not null,
	air_pressure varchar(200) not null,
	humidity varchar(200) not null,
    visibility varchar(200) not null,
    predictability varchar(200) not null,
	unique key idd (id)
    );
    drop table cities;
    Create table cities (
    woeid varchar(10) not null
);